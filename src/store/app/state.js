export default function () {
  return {
    data: {
      id: null,
      name: null,
      status: null
    },
    processes: []
  }
}
