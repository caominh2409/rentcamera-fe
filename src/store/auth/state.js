export default function () {
  return {
    access_token: null,
    account: null,
    roles: null
  }
}
