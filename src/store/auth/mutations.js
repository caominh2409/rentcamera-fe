export const signed_in = (state, data) => {
  state.access_token = data.access_token
  state.account = data.account
  state.roles = data.roles
}

export const signed_out = (state) => {
  state.access_token = null
  state.account = null
  state.roles = null
}
