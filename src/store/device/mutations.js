export const online = (state, value) => {
  state.online = value
}

export const background = (state, value) => {
  state.background = value
}

export const screen_size = (state, value) => {
  state.screen_size = value
}
