export const device = state => state
export const is_mobile = state => ['xs', 'sm'].includes(state.screen_size)
