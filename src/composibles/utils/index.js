import moment from 'moment'

export const validate_date_format = (value, format) => {
  return moment(value, format, true).isValid()
}

export const change_datetime_format = (string_date, from_format, to_format) => {
  if (!string_date) return null

  let date
  if (from_format === 'iso') {
    date = moment(string_date)
  } else {
    date = moment(string_date, from_format)
  }

  if (to_format === 'iso') {
    return date.format()
  }
  return date.format(to_format)
}

export const object_to_array = (obj, key_field, value_field) => {
  let result = []
  Object.keys(obj).forEach(k => {
    result.push({
      [key_field]: k,
      [value_field]: obj[k]
    })
  })
  return result
}

export const array_to_object = (array, key_field, value_field) => {
  let result = {}
  array.forEach(i => {
    let key = i[key_field]
    if (key) {
      result[key] = i[value_field]
    }
  })
  return result
}
