export default [
  {
    name: 'main',
    path: '/',
    component: () => import('pages/main/index.vue'),
    redirect: {name: 'main.bookings'},
    meta: {
      auth: true
    },
    children: [
      {
        name: 'main.suppliers',
        path: 'suppliers',
        meta: {
          label: 'Suppliers',
          icon: 'settings_input_component'
        },
        component: () => import('pages/main/suppliers'),
      },
      {
        name: 'main.bookings',
        path: 'bookings',
        meta: {
          label: 'Bookings',
          icon: 'bookmarks'
        },
        component: () => import('pages/main/bookings'),
      },
      {
        name: 'main.booking',
        path: 'bookings/:id',
        component: () => import('pages/main/booking'),
        meta: {
          label: 'Booking',
          icon: 'bookmarks'
        },
      },
      {
        name: 'main.loggings',
        path: 'loggings',
        meta: {
          label: 'Loggings',
          icon: 'manage_search'
        },
        component: () => import('pages/main/loggings'),
      },
      {
        name: 'main.iam',
        path: 'iam',
        meta: {
          label: 'IAM',
          icon: 'admin_panel_settings'
        },
        component: () => import('pages/main/iam'),
      },
      {
        name: 'main.markups',
        path: 'markups',
        meta: {
          label: 'markups',
          icon: 'keyboard_double_arrow_up'
        },
        component: () => import('pages/main/markups'),
      },
    ]
  },
  {
    name: 'auth',
    path: '/auth',
    component: () => import('pages/auth.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/e404.vue')
  }
]
